import 'dart:io';
import 'dart:ui' as ui;

import 'package:flutter/material.dart' hide Action;
import 'package:flutter/services.dart';

import 'image_crop.dart';

class WooPlusPhotoCrop extends StatefulWidget {
  final File photo;
  final ImageProvider decorator;
  final EdgeInsets cropPadding;
  final GlobalKey<CropState> cropKey;
  final double maximumScale;
  final disabled;
  final ScaleCompletedListener scaleCompletedListener;

  WooPlusPhotoCrop({
    @required this.cropKey,
    @required this.photo,
    this.decorator,
    this.cropPadding,
    this.maximumScale,
    this.disabled = false,
    this.scaleCompletedListener,
  });

  @override
  State<StatefulWidget> createState() {
    return WooPlusPhotoCropState();
  }
}

class WooPlusPhotoCropState extends State<WooPlusPhotoCrop> {
  ui.Image _decoratorImage;
  ImageStream _decoratorImageStream;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _getDecoratorImage();
  }

  @override
  void didUpdateWidget(WooPlusPhotoCrop oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (_decoratorImage == null) _getDecoratorImage();
  }

  _getDecoratorImage() {
    if (widget.decorator == null) return;

    final oldImageStream = _decoratorImageStream;
    _decoratorImageStream =
        widget.decorator.resolve(createLocalImageConfiguration(context));
    if (_decoratorImageStream.key != oldImageStream?.key) {
      oldImageStream
          ?.removeListener(ImageStreamListener(_updateDecoratorImage));
      _decoratorImageStream
          .addListener(ImageStreamListener(_updateDecoratorImage));
    }
  }

  _updateDecoratorImage(ImageInfo imageInfo, bool synchronousCall) {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      if (mounted) {
        setState(() {
          _decoratorImage = imageInfo.image;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Stack(
      children: <Widget>[
        SizedBox.expand(
          child: Container(
            child: _Crop.file(
              widget.photo,
              disabled: widget.disabled,
              padding: widget.cropPadding,
              key: widget.cropKey,
              aspectRatio: 1.0 / 1.0,
              maximumScale: widget.maximumScale,
              scaleCompletedListener: widget.scaleCompletedListener,
              cropPainterBuilder: (state) {
                return PlainCropPainter(
                    state: state..decoratorImage = _decoratorImage);
              },
            ),
          ),
        ),
      ],
    ));
  }
}

const _kCropOverlayActiveOpacity = 0.3;
const _kCropOverlayInactiveOpacity = 0.7;

class PlainCropPainter extends AbstractCropPainter {
  PlainCropPainter({
    CropPaintState state,
  }) : super(state: state);

  @override
  void paint(Canvas canvas, Size size) {
    final rect = Rect.fromLTWH(
      0,
      0,
      size.width,
      size.height,
    );

    canvas.save();
    canvas.translate(rect.left, rect.top);

    final paint = Paint()..isAntiAlias = false;

    if (state.image != null) {
      final src = Rect.fromLTWH(
        0.0,
        0.0,
        state.image.width.toDouble(),
        state.image.height.toDouble(),
      );
      final dst = Rect.fromLTWH(
        rect.width * state.area.left -
            state.image.width * state.view.left * state.scale * state.ratio,
        rect.height * state.area.top -
            state.image.height * state.view.top * state.scale * state.ratio,
        state.image.width * state.scale * state.ratio,
        state.image.height * state.scale * state.ratio,
      );

      canvas.save();
      canvas.clipRect(Rect.fromLTWH(0.0, 0.0, rect.width, rect.height));
      canvas.drawImageRect(state.image, src, dst, paint);
      canvas.restore();
    }

    paint.color = Color.fromRGBO(
        0x0,
        0x0,
        0x0,
        _kCropOverlayActiveOpacity * state.active +
            _kCropOverlayInactiveOpacity * (1.0 - state.active));
    final boundaries = Rect.fromLTWH(
      rect.width * state.area.left,
      rect.height * state.area.top,
      rect.width * state.area.width,
      rect.height * state.area.height,
    );

    canvas.drawRect(Rect.fromLTRB(0.0, 0.0, rect.width, boundaries.top), paint);
    canvas.drawRect(
        Rect.fromLTRB(0.0, boundaries.bottom, rect.width, rect.height), paint);
    canvas.drawRect(
        Rect.fromLTRB(0.0, boundaries.top, boundaries.left, boundaries.bottom),
        paint);
    canvas.drawRect(
        Rect.fromLTRB(
            boundaries.right, boundaries.top, rect.width, boundaries.bottom),
        paint);

    paintDecorator(canvas, paint, boundaries);

    canvas.restore();
  }

  void paintDecorator(Canvas canvas, Paint paint, Rect boundaries) {
    if (state.decoratorImage == null) return;

    canvas.save();
    paint.color = Color.fromRGBO(
        0x0,
        0x0,
        0x0,
        _kCropOverlayActiveOpacity * (1 - state.active) +
            _kCropOverlayInactiveOpacity * state.active);

    ui.Image image = state.decoratorImage;
    canvas.drawImageRect(
        image,
        Rect.fromLTWH(0, 0, image.width.toDouble(), image.height.toDouble()),
        boundaries,
        paint);
    canvas.restore();
  }
}

class _Crop extends Crop {
  final EdgeInsets padding;

  const _Crop(
      {Key key,
      this.padding,
      ImageProvider image,
      double aspectRatio,
      double maximumScale: 2.0,
      bool alwaysShowGrid: false,
      CropPainterBuilder cropPainterBuilder,
      bool disabled: false,
      ScaleCompletedListener scaleCompletedListener})
      : super(
            key: key,
            image: image,
            aspectRatio: aspectRatio,
            maximumScale: maximumScale,
            alwaysShowGrid: alwaysShowGrid,
            cropPainterBuilder: cropPainterBuilder,
            disabled: disabled,
            adjustable: false,
            scaleCompletedListener: scaleCompletedListener);

  _Crop.file(File file,
      {Key key,
      this.padding,
      double scale = 1.0,
      double aspectRatio,
      double maximumScale: 2.0,
      bool alwaysShowGrid: false,
      CropPainterBuilder cropPainterBuilder,
      bool disabled: false,
      ScaleCompletedListener scaleCompletedListener})
      : super.file(file,
            key: key,
            scale: scale,
            aspectRatio: aspectRatio,
            maximumScale: maximumScale,
            alwaysShowGrid: alwaysShowGrid,
            cropPainterBuilder: cropPainterBuilder,
            disabled: disabled,
            adjustable: false,
            scaleCompletedListener: scaleCompletedListener);

  _Crop.asset(String assetName,
      {Key key,
      this.padding,
      AssetBundle bundle,
      String package,
      double aspectRatio,
      double maximumScale: 2.0,
      bool alwaysShowGrid: false,
      CropPainterBuilder cropPainterBuilder,
      bool disabled: false,
      ScaleCompletedListener scaleCompletedListener})
      : super.asset(assetName,
            key: key,
            bundle: bundle,
            package: package,
            aspectRatio: aspectRatio,
            maximumScale: maximumScale,
            alwaysShowGrid: alwaysShowGrid,
            cropPainterBuilder: cropPainterBuilder,
            disabled: disabled,
            adjustable: false,
            scaleCompletedListener: scaleCompletedListener);

  Rect calculateDefaultArea({
    Size boundaries,
    int imageWidth,
    int imageHeight,
    double viewWidth,
    double viewHeight,
  }) {
    if (imageWidth == null || imageHeight == null) {
      return Rect.zero;
    }

    final dl = padding == null ? 0 : padding.left / boundaries.width;
    final dr = padding == null ? 0 : padding.right / boundaries.width;
    final dt = padding == null ? 0 : padding.top / boundaries.height;
    final db = padding == null ? 0 : padding.bottom / boundaries.height;

    final width = 1.0;
    final height = (imageWidth * viewWidth * width) /
        (imageHeight * viewHeight * (aspectRatio ?? 1.0));
    return Rect.fromLTWH(
      (1.0 - width) / 2 + dl,
      (1.0 - height) / 2 + dt,
      width - dl - dr,
      height - dt - db,
    );
  }
}
